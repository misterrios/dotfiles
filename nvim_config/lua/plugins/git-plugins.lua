return {
  -- show git blame
  {
    "f-person/git-blame.nvim",
    name = "git-blame",
    event = "BufRead",
    config = function()
      vim.cmd "highlight default link gitblame SpecialComment"
      vim.g.gitblame_enabled = 1
    end,
  },
  -- fugitive for vim
  {
    "tpope/vim-fugitive",
    name = "vim-fugitive",
    cmd = {
      "G",
      "Git",
      "Gdiffsplit",
      "Gread",
      "Gwrite",
      "Ggrep",
      "GMove",
      "GDelete",
      "GBrowse",
      "GRemove",
      "GRename",
      "Glgrep",
      "Gedit",
    },
    ft = { "fugitive" },
  },
  -- gitgutter
  { "airblade/vim-gitgutter", name="vim-gitgutter", },
}
