This is my collection of dotfiles and configurations. I clone this repo to my home folder
and then use symbolic links to put the items in their place, while still retaining
version control and so I can use the same configs across all my computers.

sudo ln -s ~/dotfiles/.tmux.conf ~/.tmux.conf

sudo ln -s ~/dotfiles/.zshrc ~/.zshrc

sudo ln -s ~/dotfiles/.aliases ~/.aliases

sudo ln -s ~/dotfiles/.pdbrc.py ~/.pdbrc.py

For the AstroNVim config:

sudo ln -s ~/dotfiles/nvim_config/ ~/.config/nvim  


Deprecated: 

From before I switched to Lunar Vim

sudo ln -s ~/dotfiles/init.vim ~/.config/nvim/init.vim

sudo ln -s ~/dotfiles/.vimrc ~/.vimrc

The said LunarVim configuration superseded by AstroNVim

sudo ln -s ~/dotfiles/config.lua ~/.config/lvim/config.lua 
