set nocompatible              " be iMproved, required
filetype off                  " required

" Specify a directory for plugins
call plug#begin('~/.vim/plugged')

"" AUTOCOMPLETE
" YCM for Auto Complete
Plug 'Valloric/YouCompleteMe'

"" APPEARANCE
" Monokai Color Scheme
Plug 'patstockwell/vim-monokai-tasty'
" Airline as powerline replacement
Plug 'vim-airline/vim-airline'
" Airline themes
Plug 'vim-airline/vim-airline-themes'

"" COMMENT TOGGLE
" Commenting tool
"Plug 'scrooloose/nerdcommenter'
Plug 'tpope/vim-commentary'

"" TEXT EDITING
" Makes vim act like a text editor
Plug 'reedes/vim-pencil'
" Text exchange
Plug 'tommcdo/vim-exchange'

"" FORMATTING
" auto indentation
Plug 'vim-scripts/indentpython.vim'
" vim-autoclose
Plug 'spf13/vim-autoclose'
" Black plugin
Plug 'ambv/black'
" Pep8 checking
Plug 'nvie/vim-flake8'
" async support for neovim- replaces validator for on the fly linting
Plug 'neomake/neomake'
" Syntax Checking
Plug 'scrooloose/syntastic'
" to add quotes around words or phrases
Plug 'tpope/vim-surround'
" to allow surround to repeat with .
Plug 'tpope/vim-repeat'
" isort
Plug 'stsewd/isort.nvim', { 'do': ':UpdateRemotePlugins' }
" Editor Config to use predefined project style config files
Plug 'editorconfig/editorconfig-vim'

"" DISPLAY
" rainbow colors on parentheses based on nesting level
Plug 'luochen1990/rainbow'
" Jinja syntax highlighting
Plug 'lepture/vim-jinja'

"" GIT
" Git Integration
Plug 'tpope/vim-fugitive'
" Extend Fugitive
Plug 'tommcdo/vim-fugitive-blame-ext'
" to show git changes in gutter
Plug 'airblade/vim-gitgutter'
" floating vim git message windows
Plug 'rhysd/git-messenger.vim'

"" SEARCH
" ack in vim- supports Ag
Plug 'mileszs/ack.vim'
" fzf plugin for vim
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

"" HELPERS
" sublime-like multiple selection
Plug 'terryma/vim-multiple-cursors'
" disable arrow keys
Plug 'mrmargolis/dogmatic.vim'
" highlight instances of word under cursor
Plug 'RRethy/vim-illuminate'
" Open and toggle a terminal window
Plug 'voldikss/vim-floaterm'
" Guake-like terminal window
Plug 'Lenovsky/nuake'
" SimpylFold for code folding
Plug 'tmhedberg/SimpylFold'
" Swap Arguments
Plug 'machakann/vim-swap'

"" UNDO
" Visualize undo tree
Plug 'simnalamburt/vim-mundo'

"" PYTHON
" Deal with Virtual environments
Plug 'sansyrox/vim-python-virtualenv'
" Run tests
Plug 'vim-test/vim-test'


"" INACTIVE
" " ALE plugin - Asynchronous Lint Engine
" Plug 'dense-analysis/ale'
" " expand region to faster select words and lines
" Plug 'terryma/vim-expand-region'
" " to show git changes in nerdtree
" Plug 'Xuyuanp/nerdtree-git-plugin'
" " multi-language hightlight support
" Plug 'sheerun/vim-polyglot'
" " arguments swapping and deleting made easy
" Plug 'PeterRincker/vim-argumentative.git'
" " Open python modules in vim using ped
" Plug 'sloria/vim-ped'
" " pulse when showing search results
" Plug 'numirias/semshi'

call plug#end()

"" Non-Plugin Stuff goes here

" make backspace work line normal
set backspace=indent,eol,start

autocmd BufWritePost *.py call flake8#Flake8()

au BufNewFile,BufRead *.py :
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    "\ set textwidth=90 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix |

au BufNewFile,BufRead *.js,*.html,*.css :
    \ set tabstop=2 |
    \ set softtabstop=2 |
    \ set shiftwidth=2 |


let python_highlight_all=1
syntax on
" line numbering
set nu
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab
set encoding=utf-8
set clipboard=unnamed
set colorcolumn=90
" will do case insensitive searches when lowercase, sensitive when uppercase
set smartcase

"" COLORSCHEME 
colorscheme vim-monokai-tasty

" show filepaths
set title
set ls=2

" code folding
set foldmethod=indent
set foldnestmax=10
set nofoldenable
set foldlevel=1

" persistent undos (from Mastering Vim Quickly)
" Maintain undo history between sessions
set undofile
set undodir=~/.vim/undodir

" use one virtualenv specifically for neovim
let g:python3_host_prog="~/.virtualenvs/neovim/bin/python"
let g:python_host_prog="~/.virtualenvs/neovim_python2/bin/python"


" guide settings
set ts=4 sw=4
let g:indent_guides_start_level=2
let g:indent_guides_guide_size=1
let g:indent_guides_color_change_percent = 90

" map leader key to spacebar
let mapleader = "\<Space>"

" activate rainbow highlighting
let g:rainbow_active = 1

" frequent leader actions
" https://sheerun.net/2014/03/21/how-to-boost-your-vim-productivity/
"" save file
nnoremap <Leader>w :w<CR>
"" run Black and save
nnoremap <Leader>b :Black<CR> <bar> :w<CR>
"" cut and paste from clipboard
vmap <Leader>y "+y
vmap <Leader>d "+d
nmap <Leader>p "+p
nmap <Leader>P "+P
vmap <Leader>p "+p
vmap <Leader>P "+P
"" visual line mode with space space
nmap <Leader><Leader> V
"" vim-gitgutter: jump to next/previous git change
nmap <Leader>h <Plug>(GitGutterNextHunk)
nmap <Leader>H <Plug>(GitGutterPrevHunk)
" paste from unnamed register into current (black hole'd) line
nnoremap <Leader>r "_ddP

let g:pymode_python = 'python3'

set runtimepath^=~/.vim/bundle/ctrlp.vim

" controlP settings
let g:ctrlp_custom_ignore = { 
    \  'dir':  '\.(git|hg|svn)$\|coverage$\|htmlcov$\|node_modules$\|bower_components$',
    \ 'file': '\v\.(exe|so|dll|sql|orig|pyc|jpg|png|pdf|mp4|webm)$',
    \}          

" wildignore to let Ack and controlP ignore stuff
set wildignore+=*/htmlcov/*,*/dist/*,*/node_modules/*,*o,*.obj,*.bak,*.exe,*.py[co],*.swp,*~,*.pyc,.svn

" Point Vim-Ack to use Ag
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif

" You Complete Me Optimizations
let g:ycm_server_keep_logfiles = 1

let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>
let g:ycm_semantic_triggers = {
	\   'python': [ 're!(import\s+|from\s+(\w+\s+(import\s+(\w+,\s+)*)?)?)' ]
	\ }

"Syntastic settings
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" vim-flake8 settings
let g:flake8_show_in_gutter=1
let g:flake8_show_in_file=1
let g:flake8_show_quickfix=0

" validator.vim settings
let g:validator_python_checkers = ['flake8']

" vim-expand-region-settings
vmap v <Plug>(expand_region_expand)
vmap <C-v> <Plug>(expand_region_shrink)

" easier split navigations
" https://robots.thoughtbot.com/vim-splits-move-faster-and-more-naturally
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
nnoremap <C-X> <C-W><C-X>

" open nuake terminal
map <leader>t :Nuake<CR>

" multiple-cursor settings
let g:multi_cursor_use_default_mapping=0
let g:multi_cursor_start_word_key      = '<C-n>'
let g:multi_cursor_select_all_word_key = '<A-n>'
let g:multi_cursor_start_key           = 'g<C-n>'
let g:multi_cursor_select_all_key      = 'g<A-n>'
let g:multi_cursor_next_key            = '<C-n>'
let g:multi_cursor_prev_key            = '<C-l>'
let g:multi_cursor_skip_key            = '<C-s>'
let g:multi_cursor_quit_key            = '<Esc>'

set splitbelow
set splitright

set guifont=Victor\ Mono\ 18

" show title when using tmux
" http://stackoverflow.com/questions/15123477/tmux-tabs-with-name-of-file-open-in-vim
autocmd BufEnter * let &titlestring = ' ' . expand("%:t")
set title

"html django omnicomplete function
au FileType htmldjango set omnifunc=htmldjangocomplete#CompleteDjango
let g:htmldjangocomplete_html_flavour = 'html5'

"auto complete for django tags
au FileType htmldjango inoremap {% {%  %}<left><left><left>
au FileType htmldjango inoremap {{ {{  }}<left><left><left>
au FileType htmldjango inoremap {/ {%  %}<left><left><left>
" :ia pdb import pdb; pdb.set_trace()
:ia pdb breakpoint()

"add suffixes for python imports
:set suffixesadd+=.py

" italicize comments
highlight Comment cterm=italic

" Airline settings
let g:airline_powerline_fonts = 1
" let g:airline_theme = "vim-monokai-tasty"

" black settings
" virtualenv specifically for black
let g:black_virtualenv = "~/.virtualenvs/neovim"
let g:black_linelength = 88
let g:black_skip_string_normalization = 1

" fzf shortcut
map ; :Files<CR>

" vim-test settings
let test#python#runner = 'pytest'
let test#strategy = "neovim"
nnoremap <leader>tf :TestFile<cr>
nnoremap <leader>tn :TestNearest<cr>

" vim-floaterm settings
let g:floaterm_width = 120
let g:floaterm_position = 'center'
nnoremap <leader>f :FloatermToggle<CR>

" Show filename and path
:command Filename :echo expand('%:p')

" Mundo leader shortcut
nnoremap <leader>n :MundoToggle<CR>

" Pencil leader shortcut
nnoremap <leader>l :PencilToggle<CR>

" jinja detection
au BufNewFile,BufRead *.html,*.htm,*.shtml,*.stm,*.template,*macro set ft=jinja

" Use python 3 for isort
let g:vim_isort_python_version = 'python3'

if filereadable("~/.localvimrc")
  source "~/.localvimrc"
endif
